module firstJavaFxProject {
    requires javafx.controls;
    requires javafx.media;

    exports be.kdg.pro2.javafx;
}